#include "settings.h"

void initSettings(Settings * settings) {
	*settings = (Settings){
		.controlMode = KEYBOARD_AND_MOUSE_CONTROL,
		.mouseSensitivity = 0.0007,
		.speedMod = 10.0,
		.scrollBarSpeed = 2.0,
		.lockMouse = true,
		.gamePadNum = 0,
		.pitchStick = 1,
		.yawStick = 0,
		.rollStick = 2,
		.speedStick = 3,
		.joystickSensitivity = 0.5,
		.fps = 0.0,
		.drawFps = true,
		.renderWidth = 640,
		.renderHeight = 360,
		.useWorldRenderTexture = true,
		.previewMouseSensitivity = 0.01,
		.previewZoomSpeed = 2.0
	};
}

void applySettings(Settings * settings) {
	SetTargetFPS(settings->fps);
}
