#include "gyroscope.h"
#include "game.h"
#include "assets.h"
#include "world.h"

void initGyroscope(Gyroscope * gyroscope) {
    resetGyroscopePosition(gyroscope);

    gyroscope->camera = (Camera3D){
        .fovy = 45.0,
		.projection = CAMERA_PERSPECTIVE,
		.target = Vector3Zero(),
	};

    gyroscope->texture = LoadRenderTexture(
		GYROSCOPE_TEXTURE_SIZE,
		GYROSCOPE_TEXTURE_SIZE
	);
}

void closeGyroscope(Gyroscope * gyroscope) {
    UnloadRenderTexture(gyroscope->texture);
}

void resetGyroscopePosition(Gyroscope * gyroscope) {
    float width = GetScreenWidth();
	float height = GetScreenHeight();

    // Set position on screen.
    gyroscope->position = (Vector2){
        (width / 2.0) - (GYROSCOPE_TEXTURE_SIZE / 2.0),
		height / 1.8
    };
}

void updateGyroscopeRotation(Entity * player, Gyroscope * gyroscope) {
	// Instead of rotating the gyroscope ball we rotate the camera around it.
	gyroscope->camera.position = Vector3Scale(
		Vector3RotateByQuaternion((Vector3){0.0, 0.0, 1.0}, player->rotation),
		GYROSCOPE_CAMERA_DISTANCE
	);

	gyroscope->camera.up = Vector3RotateByQuaternion((Vector3){0.0, 1.0, 0.0}, player->rotation);
}

void drawGyroscope(Game * game, Gyroscope * gyroscope) {
	Entity * player = getEntityFromWorld(game->world, 0);

	updateGyroscopeRotation(player, gyroscope);

	// Get model
	Model model = game->assets.models[GYROSCOPE_ASSET];

	// Draw this mother fucker.
	BeginTextureMode(gyroscope->texture);
	ClearBackground((Color){0x0, 0x0, 0x0, 0x0});
	BeginMode3D(gyroscope->camera);

	// This this fucker!!!
	DrawModel(model, Vector3Zero(), 1, WHITE);

	EndMode3D();
	EndTextureMode();

	// Draw render texture.
	DrawTextureV(gyroscope->texture.texture, gyroscope->position, GYROSCOPE_TEXTURE_COLOR);

	// Top to bottom.
	DrawLineEx(
		(Vector2){
			gyroscope->position.x + (GYROSCOPE_TEXTURE_SIZE / 2.0),
			gyroscope->position.y,
		},
		(Vector2){
			gyroscope->position.x + (GYROSCOPE_TEXTURE_SIZE / 2.0),
			gyroscope->position.y + GYROSCOPE_TEXTURE_SIZE,
		},
		GYROSCOPE_LINE_THICKNESS,
		SKYBLUE
	);

	// Left to right.
	DrawLineEx(
		(Vector2){
			gyroscope->position.x,
			gyroscope->position.y + (GYROSCOPE_TEXTURE_SIZE / 2.0),
		},
		(Vector2){
			gyroscope->position.x + GYROSCOPE_TEXTURE_SIZE,
			gyroscope->position.y + (GYROSCOPE_TEXTURE_SIZE / 2.0),
		},
		GYROSCOPE_LINE_THICKNESS,
		YELLOW
	);
}
