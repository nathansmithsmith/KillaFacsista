#include "gameCommon.h"

#ifndef STARS_H
#define STARS_H

#define STAR_COUNT 32

#define STAR_MIN_DISTANCE 400.0
#define STAR_MAX_DISTANCE 1000.0

typedef struct Stars {
    Vector3 stars[STAR_COUNT];
    bool starsUsed[STAR_COUNT];
} Stars;

void initStars(Stars * stars);
void drawStars(Game * game, Stars * stars);

#endif
