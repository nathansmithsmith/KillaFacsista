#include "killLog.h"
#include "game.h"
#include "entity.h"

void initKillLog(KillLog * killLog) {
   killLog->killCount = 0;
}

void pushKill(KillLog * killLog, Entity entity) {
    int i;

    Kill kill = (Kill){
        .type = entity.type,
        .id = entity.id,
        .fingerPrint = entity.fingerprint,
        .timeAtKill = GetTime()
    };

    ++killLog->killCount;

    // Push shit back if at max size.
    if (killLog->killCount > KILL_LOG_MAX) {
        for (i = 0; i < KILL_LOG_MAX - 1; ++i)
            killLog->kills[i] = killLog->kills[i + 1];

        killLog->killCount = KILL_LOG_MAX;
    }

    killLog->kills[killLog->killCount - 1] = kill;
}

void formatKillMessage(char killMessage[KILL_MESSAGE_MAX], Kill kill) {
    // Get time since kill in minutes and seconds.
    double timeSinceKill = GetTime() - kill.timeAtKill;
    int minutes = timeSinceKill / 60;
    int seconds = (int)timeSinceKill % 60;

    // Nicely format.
    snprintf(killMessage, KILL_MESSAGE_MAX, "%s@%x %d:%d", entityTypeInfo[kill.type].name, kill.fingerPrint, minutes, seconds);
}

void drawKillLog(KillLog * killLog, Vector2 position) {
    int i;
    char killMessage[KILL_MESSAGE_MAX];

    // Format and draw each kill.
    for (i = 0; i < killLog->killCount; ++i) {
        formatKillMessage(killMessage, killLog->kills[i]);

        DrawText(
            killMessage,
            position.x,
            position.y + (KILL_LOG_FONT_SIZE * i),
            KILL_LOG_FONT_SIZE,
            WHITE
        );
    }
}

void resetKillLog(KillLog * killLog) {
    killLog->killCount = 0;
}
