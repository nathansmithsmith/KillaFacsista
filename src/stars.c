#include "stars.h"
#include "world.h"
#include "game.h"
#include "entity.h"

void initStars(Stars * stars) {
    int i;

    for (i = 0; i < STAR_COUNT; ++i)
        stars->starsUsed[i] = false;
}

Vector3 generateStar(unsigned int seed) {
    SetRandomSeed(seed);

    // Get direction.
    Vector3 star = (Vector3){
        GetRandomValue(1000, -1000),
        GetRandomValue(1000, -1000),
        GetRandomValue(1000, -1000)
    };

    star = Vector3Normalize(star);

    // Move far away.
    float distance = STAR_MAX_DISTANCE;
    star = Vector3Scale(star, distance);

    return star;
}

void drawStars(Game * game, Stars * stars) {
    int i;

    Entity * player = getEntityFromWorld(game->world, 0);

    for (i = 0; i < STAR_COUNT; ++i) {
        if (stars->starsUsed[i]) {
            DrawSphereEx(stars->stars[i], 1.0 - (Vector3Distance(stars->stars[i], player->position) * 0.005), 4, 4, WHITE);

            // Flag for reset if to close or far.
            float distance = Vector3Distance(stars->stars[i], player->position);

            if (distance < STAR_MIN_DISTANCE || distance > STAR_MAX_DISTANCE)
                stars->starsUsed[i] = false;
        } else { // Reset star.
            stars->starsUsed[i] = true;
            stars->stars[i] = Vector3Add(player->position, generateStar(time(NULL) + i));
        }
    }
}
