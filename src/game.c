#include "game.h"

void initGame(Game * game) {
	// Window.
	InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Killa Facsista");

	SetWindowState(FLAG_WINDOW_RESIZABLE);

	// Settings.
	initSettings(&game->settings);
	applySettings(&game->settings);

	// Assets.
	LoadAssets(&game->assets);

	// Screen id.
	game->screenId = SCREEN_MAIN_MENU;

	// Main menu.
	initMainMenu(game);

	// How to play screen.
	initHowToPlayScreen(game);

	// Info screen.
	initInfoScreen(game);

	// Camera.
	initCameras(game, game->cameras);

	// Game screen.
	initGameScreen(game, &game->gameScreen);
	
	// World.
	initWorld(&game->world);

	// Kill log lmao.
	initKillLog(&game->killLog);

	// Levels.
	initLevels(&game->levels);
	startLevel(game, &game->levels, 0);
}

void closeGame(Game * game) {
	unloadAssets(&game->assets);
	freeWorld(&game->world);
	freeGameScreen(&game->gameScreen);

	endLevel(game, &game->levels);

	// Close window last.
	CloseWindow();
}

void gameResize(Game * game) {
	resizeGameScreen(game, &game->gameScreen);
	resizeMainMenu(game, &game->mainMenu);
	resizeHowToPlayScreen(game, &game->howToPlayScreen);
}

void updateGame(Game * game) {
	BeginDrawing();

	switch (game->screenId) {
		case SCREEN_MAIN_MENU:
			updateMainMenu(game);
			break;
		case SCREEN_GAME:
			updateGameScreen(game);
			break;
		case HOW_TO_PLAY_SCREEN:
			updateHowToPlayScreen(game);
			break;
		case INFO_SCREEN:
			updateInfoScreen(game);
			break;
		default:
			break;
	}

	if (game->settings.drawFps)
		DrawFPS(5, 5);

	if (IsWindowResized())
		gameResize(game);

	EndDrawing();
}
