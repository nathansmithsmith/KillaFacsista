#include "gameCommon.h"
#include "gyroscope.h"
#include "radar.h"
#include "cameras.h"
#include "stars.h"

#ifndef GAME_SCREEN_H
#define GAME_SCREEN_H

#define GAME_SCREEN_TEXT_SIZE 20.0
#define GAME_SCREEN_NEXT_LEVEL_DELAY 4.0
#define GAME_SCREEN_TARGET_INFO_MAX 20

#define GAME_SCREEN_ZOOM_VIEW_SIZE 100 // Size it renders at.
#define GAME_SCREEN_ZOOM_VIEW_UI_SIZE 350 // Size on screen.

#define GAME_SCREEN_SKY_BOX_SIZE 1020.0

#define GAME_SCREEN_SHOW_GAME_OVER_FOR 4.0
#define GAME_SCREEN_GAME_OVER_FONT_SIZE 100

#define GAME_SCREEN_GAME_COMPLETE_SHOW_FOR 6.0
#define GAME_SCREEN_GAME_COMPLETE_FONT_SIZE 100

// Gui stuff and shit.
typedef struct GameScreen {
    Vector2 infoTextPosition;
    Vector2 targetInfoPosition;
    Vector2 killLogPosition;

    Gyroscope gyroscope;
    Radar radar;
    CameraId mainCamera;

	bool levelComplete;
	double timeAtLevelComplete;
	int lastLevel;
    int nextLevelInsultNum;

    // Since the player entity gets reallocated each level we use this to remember its health.
    float healthAtLevelEnd;

    bool gameOver;
    double gameOverAt;

    bool gameComplete;
    double gameCompleteAt;

    RenderTexture zoomViewTexture;
    Vector2 zoomViewPosition;

    RenderTexture worldRender;
    bool usingWorldRenderTexture;

    Stars stars;
} GameScreen;

void initGameScreen(Game * game, GameScreen * gameScreen);
void freeGameScreen(GameScreen * gameScreen);

void updateGameScreen(Game * game);
void resizeGameScreen(Game * game, GameScreen * gameScreen);

void openGameScreen(Game * game);

// Call before setting screen when in game screen
void closeGameScreen(Game * game);

#endif
