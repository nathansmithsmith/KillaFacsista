#include "gameCommon.h"
#include "entity.h"
#include "assets.h"

#ifndef INFO_SCREEN_H
#define INFO_SCREEN_H

#define ENTITY_USER_INFO_MSG_MAX 70
#define INFO_SCREEN_ENTITY_COUNT 7

#define ENTITY_INFO_NAME_FONT_SIZE 30
#define ENTITY_INFO_FONT_SIZE 18

typedef struct EntityUserInfo {
    EntityType type;
    AssetId assetId;
    float cameraDistance;
    char msg[ENTITY_USER_INFO_MSG_MAX];
} EntityUserInfo;

extern const EntityUserInfo infoScreenEntityInfo[INFO_SCREEN_ENTITY_COUNT];

typedef struct InfoScreen {
    Rectangle goBackButton; // Go to main menu.
    Rectangle backButton;
    Rectangle nextButton;
    Rectangle resetButton;

    Vector2 controlsPosition;

    Camera3D camera;
    float distances[ENTITY_USER_INFO_MSG_MAX];

    int currentEntity;

    Vector2 lastMouse;
    Quaternion rotations[ENTITY_USER_INFO_MSG_MAX];
    bool resetMouse;
} InfoScreen;

void initInfoScreen(Game * game);
void updateInfoScreen(Game * game);
void resizeInfoScreen(Game * game, InfoScreen * infoScreen);

#endif
