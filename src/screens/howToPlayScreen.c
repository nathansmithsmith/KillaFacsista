#include "howToPlayScreen.h"
#include "game.h"

void initHowToPlayScreen(Game * game) {
    HowToPlayScreen * howToPlayScreen = &game->howToPlayScreen;

    howToPlayScreen->goBackButton = (Rectangle){0.0, 25.0, 100.0, 50.0};
    howToPlayScreen->infoText = (Rectangle){0.0, 0.0, 0.0, 0.0};

    resizeHowToPlayScreen(game, howToPlayScreen);
}

void updateHowToPlayScreen(Game * game) {
    HowToPlayScreen * howToPlayScreen = &game->howToPlayScreen;

    ClearBackground(RAYWHITE);

    // Back to main menu button.
    if (GuiButton(howToPlayScreen->goBackButton, "Back"))
        game->screenId = SCREEN_MAIN_MENU;

    // Info text.
    const char info[] =
    "Game play:\n"
    "   Kill fascist space ships and move on (:\n"
    "Controls:\n"
    "   Sorry no wasd ):\n"
    "   Mouse to look around normally\n"
    "   Middle click with mouse movement to rotate\n"
    "   Scroll wheel to change the ship speed\n"
    "   Shift to slow down the mouse and scroll wheel\n"
    "   Ctrl to speed up the moues and scroll wheel\n"
    "   Number key to change the camera view\n"
    "   e to end game\n"
    "   Left click to shoot\n"
    "   Right click to auto target\n"
    "Using auto target:\n"
    "   Hitting things is hard so use the auto target pleaz (:\n"
    "   It will auto target on anything that is in range\n"
    "   Auto target will stop if you are out of range\n"
    "   The color will change from blue to red when you can kill\n"
    "   For some types of color blindness use the \"On Target\" instead\n"
    "   Try to manually aim to help the auto target";

    DrawText(info, howToPlayScreen->infoText.x + 3, howToPlayScreen->infoText.y + 3, 20, BLACK);
    DrawRectangleLinesEx(howToPlayScreen->infoText, 2, BLUE);
}

void resizeHowToPlayScreen(Game * game, HowToPlayScreen * howToPlayScreen) {
    int width = GetScreenWidth();
    int height = GetScreenHeight();

    howToPlayScreen->infoText.width = width / 1.5;
    howToPlayScreen->infoText.height = height / 1.5;
    howToPlayScreen->infoText.x = (width / 2.0) - (howToPlayScreen->infoText.width / 2.0);
    howToPlayScreen->infoText.y = (height / 2.0) - (howToPlayScreen->infoText.height / 2.0);
}
