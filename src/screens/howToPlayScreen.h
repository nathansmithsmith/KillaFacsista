#include "gameCommon.h"

#ifndef HOW_TO_PLAY_SCREEN_H
#define HOW_TO_PLAY_SCREEN_H

typedef struct HowToPlayScreen {
    Rectangle goBackButton;
    Rectangle infoText;
} HowToPlayScreen;

void initHowToPlayScreen(Game * game);
void updateHowToPlayScreen(Game * game);
void resizeHowToPlayScreen(Game * game, HowToPlayScreen * howToPlayScreen);

#endif
