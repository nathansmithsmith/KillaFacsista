#include "gameCommon.h"

#ifndef MAIN_MENU_H
#define MAIN_MENU_H

typedef struct MainMenu {
	Vector2 logoPosition;
	Texture2D * logoTexture;

	Rectangle startButton;
	Rectangle howToPlayButton;
	Rectangle infoButton;
} MainMenu;

void initMainMenu(Game * game);
void updateMainMenu(Game * game);
void resizeMainMenu(Game * game, MainMenu * mainMenu);

#endif
