#include "mainMenu.h"
#include "game.h"
#include "gameScreen.h"

void initMainMenu(Game * game) {
	game->mainMenu = (MainMenu){
		.startButton = (Rectangle){0.0, 0.0, 100.0, 50.0},
		.howToPlayButton = (Rectangle){0.0, 0.0, 100.0, 50.0},
		.infoButton = (Rectangle){0.0, 0.0, 100.0, 50.0},
		.logoTexture = &game->assets.textures[ICON128_ASSET]
	};

	resizeMainMenu(game, &game->mainMenu);
}

void updateMainMenu(Game * game) {
	MainMenu * mainMenu = &game->mainMenu;
	ClearBackground(RAYWHITE);

	// Logo.
	DrawTextureV(
		*mainMenu->logoTexture,
		mainMenu->logoPosition,
		WHITE
	);

	if (GuiButton(mainMenu->startButton, "Start"))
		openGameScreen(game);

	if (GuiButton(mainMenu->howToPlayButton, "How to play"))
		game->screenId= HOW_TO_PLAY_SCREEN;

	if (GuiButton(mainMenu->infoButton, "Info"))
		game->screenId = INFO_SCREEN;
}

void resizeMainMenu(Game * game, MainMenu * mainMenu) {
	int width = GetScreenWidth();
	int height = GetScreenHeight();

	// Logo.
	mainMenu->logoPosition = (Vector2){
		(width / 2.0) - (mainMenu->logoTexture->width / 2.0),
		(height / 2.0) - (mainMenu->logoTexture->height * 1.50)
	};

	// Start button.
	mainMenu->startButton.x = (width / 2.0) - (mainMenu->startButton.width / 2.0);
	mainMenu->startButton.y = (height / 2.0) - (mainMenu->startButton.height / 2.0);

	// How to play button.
	mainMenu->howToPlayButton.x = (width / 2.0) - (mainMenu->startButton.width / 2.0);
	mainMenu->howToPlayButton.y = (height / 2.0) - (mainMenu->startButton.height / 2.0);
	mainMenu->howToPlayButton.y += mainMenu->startButton.height;

	// Info button.
	mainMenu->infoButton.x = (width / 2.0) - (mainMenu->infoButton.width / 2.0);
	mainMenu->infoButton.y = mainMenu->howToPlayButton.height + mainMenu->howToPlayButton.y;
}
