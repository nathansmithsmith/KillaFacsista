#include "gameCommon.h"
#include "entity.h"
#include "bullets.h"

#ifndef ANTIFA_SHIP_H
#define ANTIFA_SHIP_H

#define ANTIFA_SHIP_MAX_SPEED 200.0
#define ANTIFA_BULLET_COOLDOWN 0.5
#define ANTIFA_DRAW_BULLET_FOR 0.05
#define ANTIFA_BULLET_DAMAGE 0.125

// Auto target shit.
#define ANTIFA_START_AUTO_TARGET_MAX 0.5 // How far off auto target can be entail it drops.
#define ANTIFA_TARGETING_SPEED 6.0

// Some keys for debugging.
//#define DEBUG_KEYS

typedef struct AntifaShip {
	Vector2 lastMouse;
	float forwardSpeed;
	bool shouldInitMousePosition;

	double timeSinceLastBullet;
	Bullet lastBulletShot;
	Vector3 gunTarget;

	bool doAutoTarget;
	bool isOnTarget;
	EntityId targetedEntityId;
	EntityFingerprint targetedEntityFingerprint;
} AntifaShip;

void initAntifaShip(Entity * entity, Game * game);
void closeAntifaShip(Entity * entity);
void updateAntifaShip(Game * game, Entity * entity);
void drawAntifaShip(Game * game, Entity * entity);

#endif
