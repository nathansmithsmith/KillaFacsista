#include "gameCommon.h"
#include "entity.h"
#include "PID.h"

#ifndef SOLDATO_H
#define SOLDATO_H

#define SOLDATO_COOLDOWN 1.0
#define SOLDATO_AIM_RADIOUS 1.5
#define SOLDATO_BULLET_DAMAGE 0.01
#define SOLDATO_GUN_TARGETING_SPEED 10.0 // 3.0
#define SOLDATO_ROTATION_SPEED 0.5

typedef struct Soldato {
	EntityFlyToPointInfo flyToPointLeading;
	EntityFlyToPointInfo flyToPointFollowing;
	double timeSinceLastShot;

	Vector3 gunTarget;
} Soldato;

void initSoldato(Entity * entity, Game * game);
void closeSoldato(Entity * entity);
void updateSoldato(Game * game, Entity * entity);
void drawSoldato(Game * game, Entity * entity);

void setSoldatoLeader(Entity * entity1, Entity * entity2);

#endif
