#include "gameCommon.h"
#include "entity.h"

// This mother fucker follows the player and goes fucking boom boom!!!
// btw I wrote some of this on 9/11

#ifndef GUIDED_MISSILE_H
#define GUIDED_MISSILE_H

#define GUIDED_MISSILE_DAMAGE 0.5
#define GUIDED_MISSILE_BOOM_BOOM_AT 5.0
#define GUIDED_MISSILE_DEATH_DAY 7.0 // Its like a birth day but sad.

typedef struct GuidedMissile {
    bool beenBorn;
    double birthDay; // Even missiles have birth days.
    EntityFlyToPointInfo flyToPoint;
} GuidedMissile;

void initGuidedMissile(Entity * entity, Game * game);
void closeGuidedMissile(Entity * entity);
void updateGuidedMissile(Game * game, Entity * entity);
void drawGuidedMissile(Game * game, Entity * entity);

// Boom boom like a poop poop
void guidedMissileGoBoomBoom(Game * game, Entity * entity);

#endif
