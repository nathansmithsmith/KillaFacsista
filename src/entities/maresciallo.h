#include "gameCommon.h"
#include "entity.h"

#ifndef MARESCIALLO_H
#define MARESCIALLO_H

#define MARESCIALLO_CIRCLE_AT_DIS 1000.0 // 200.0
#define MARESCIALLO_COME_BACK_AT_DIS 1050.0
#define MARESCIALLO_ROTATION_SPEED 20.0
#define MARESCIALLO_CIRCLE_PLAYER_SPEED 0.5

#define MARESCIALLO_BULLET_COOLDOWN 3.0
#define MARESCIALLO_BULLET_DAMAGE 0.01
#define MARESCIALLO_GUN_TARGETING_SPEED 20.0

#define MARESCIALLO_MISSILE_COOLDOWN 5.0

typedef struct Maresciallo {
	EntityFlyToPointInfo flyToPoint;
	double timeSinceLastBulletShot;
	double timeSinceLastMissileShot;

	Vector3 gunTarget;
} Maresciallo;

void initMaresciallo(Entity * entity, Game * game);
void closeMaresciallo(Entity * entity);
void updateMaresciallo(Game * game, Entity * entity);
void drawMaresciallo(Game * game, Entity * entity);

#endif
