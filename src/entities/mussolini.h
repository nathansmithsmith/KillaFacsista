#include "gameCommon.h"
#include "entity.h"
#include "PID.h"

#ifndef MUSSOLINI_H
#define MUSSOLINI_H

#define MUSSOLINI_TURN_SPEED 1.0
#define MUSSOLINI_MISSILE_DOWN_COOL 0.5

typedef struct Mussolini {
    double timeSinceLastMissile;
} Mussolini;

// Positions for the missiles to start at.
#define MUSSOLINI_LAUNCH_POSITIONS_COUNT 8
extern const Vector3 mussoliniLaunchPositions[MUSSOLINI_LAUNCH_POSITIONS_COUNT];

void initMussolini(Entity * entity, Game * game);
void closeMussolini(Entity * entity);
void updateMussolini(Game * game, Entity * entity);
void drawMussolini(Game * game, Entity * entity);

#endif
