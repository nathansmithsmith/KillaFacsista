#include "gameCommon.h"
#include "entity.h"

#ifndef MISSILE_H
#define MISSILE_H

#define MISSILE_LIVE_FOREVER -1.0

#define MISSILE_DEFAULT_SPEED 100.0
#define MISSILE_DEFAULT_TIME_TO_LIVE MISSILE_LIVE_FOREVER
#define MISSILE_DEFAULT_DAMAGE 2.5
#define MISSILE_DEFAULT_BOOM_BOOM_AT 5.0

// I WANT TO BUILD A FUCKING MISSILE IN REAL LIFE!!!

typedef struct Missile {
    double timeToLive;
    double birthDay;
    float damage;
    float boomBoomAt;
} Missile;

void initMissile(Entity * entity, Game * game);
void closeMissile(Entity * entity);
void updateMissile(Game * game, Entity * entity);
void drawMissile(Game * game, Entity * entity);

void launchMissileAtTarget(Entity * entity, Vector3 target, float speed);

void setMissileDirection(Entity * entity, Vector3 direction);
void setMissileDamage(Entity * entity, float damage);
void setMissileBoomBoomAt(Entity * entity, float boomBoomAt);

void startMissileCountDown(Entity * entity, double timeToLive);

// Ok boomer
void missileGoBoomBoom(Game * game, Entity * entity);

#endif
