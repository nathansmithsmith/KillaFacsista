#include "maresciallo.h"
#include "assets.h"
#include "game.h"
#include "PID.h"
#include "bullets.h"
#include <raylib.h>

void initMaresciallo(Entity * entity, Game * game) {
	entity->model = &game->assets.models[MARESCIALLO_ASSET];
	entity->collisionModel = entityCreateCollisionModel(*entity->model);
	entity->transformedCollisionModel = entityCreateCollisionModel(*entity->model);
	setEntityRadius(entity);

	entity->health = 0.75;

	// Allocate data.
	entity->data = KF_MALLOC(sizeof(Maresciallo));

	if (entity->data == NULL) {
		ALLOCATION_ERROR;
		return;
	}

	Maresciallo * data = (Maresciallo*)entity->data;

	data->timeSinceLastBulletShot = GetTime();
	data->timeSinceLastMissileShot = GetTime();

	PIDConfig flyToPointPID = (PIDConfig){
		.kP = 1.5,
		.kI = 0.0,
		.kD = 0.0,
		.angleMode = false,
		.doClamp = true,
		.min = 0.0,
		.max = 190.0
	};

	data->flyToPoint = (EntityFlyToPointInfo){
		.controller.speedPID = createPID(flyToPointPID),
		.controlType = ENTITY_FLY_TO_POINT_PID,
		.rotationSpeed = MARESCIALLO_ROTATION_SPEED,
		.applyRotation = true
	};

	data->gunTarget = Vector3One();
}

void closeMaresciallo(Entity * entity) {
	if (entity->data != NULL)
		KF_FREE(entity->data);

	entityFreeCollisionModel(entity->collisionModel);
	entityFreeCollisionModel(entity->transformedCollisionModel);
}

void flyToPointMaresciallo(Game * game, Entity * entity) {
	Maresciallo * data = (Maresciallo*)entity->data;
	Entity * player = getEntityFromWorld(game->world, 0);

	// Get target.
	Vector3 target = Vector3Subtract(entity->position, player->position);
	target = Vector3Normalize(target);
	target = Vector3Scale(target, MARESCIALLO_CIRCLE_AT_DIS);
	target = Vector3Add(player->position, target);

	// Fucking fly.
	entityFlyToPoint(entity, target, &data->flyToPoint);
}

void circlePlayerMaresciallo(Game * game, Entity * entity) {
	float t = GetFrameTime();
	Maresciallo * data = (Maresciallo*)entity->data;
	Entity * player = getEntityFromWorld(game->world, 0);
	
	Vector3 axis = (Vector3){1.0, 1.0, 1.0};
	float angle = t * MARESCIALLO_CIRCLE_PLAYER_SPEED;

	Vector3 oldPos = entity->position;
	
	// Get position.
	entity->position = Vector3Add(
		Vector3RotateByAxisAngle(
			Vector3Subtract(entity->position, player->position),
			axis,
			angle
		),
		player->position
	);

	Vector3 direction = Vector3Subtract(oldPos, entity->position);
	direction = Vector3Normalize(direction);

	// Get rotation.
	Matrix matrix = MatrixLookAt(Vector3Zero(), direction, (Vector3){0.0, 1.0, 0.0});
	Quaternion rotation = QuaternionInvert(QuaternionFromMatrix(matrix));
	entity->rotation = QuaternionSlerp(entity->rotation, rotation, t * MARESCIALLO_ROTATION_SPEED);
}

// A gun, like one of those things you find in public schools.
void updateGunMaresciallo(Game * game, Entity * entity) {
	double t = GetTime();
	Maresciallo * data = (Maresciallo*)entity->data;
	Entity * player = getEntityFromWorld(game->world, 0);

	// Aim at the stupid fucker playing this stupid ass game.
	Vector3 target = Vector3Normalize(Vector3Subtract(player->position, entity->position));
	data->gunTarget = Vector3Lerp(data->gunTarget, target, GetFrameTime() * MARESCIALLO_GUN_TARGETING_SPEED);

	if (t - data->timeSinceLastBulletShot < MARESCIALLO_BULLET_COOLDOWN)
		return;

	// Create and shoot bullet.
	Bullet bullet = createBulletFromDirection(*entity, data->gunTarget, MARESCIALLO_BULLET_DAMAGE);
	BulletHitInfo hit = shootBulletAtEntity(player, bullet);

	data->timeSinceLastBulletShot = t;
}

// A missile, might find in a school in florida.
void updateMissilesMaresciallo(Game * game, Entity * entity) {
	double t = GetTime();
	Maresciallo * data = (Maresciallo*)entity->data;

	if (t - data->timeSinceLastMissileShot < MARESCIALLO_MISSILE_COOLDOWN)
		return;

	Entity missile = createEntity(ENTITY_GUIDED_MISSILE, game);
	missile.position = entity->position;
	scheduleEntityToAdd(&game->world, missile);

	data->timeSinceLastMissileShot = t;
}

void updateMaresciallo(Game * game, Entity * entity) {
	entityUpdateLastValues(entity);

	Maresciallo * data = (Maresciallo*)entity->data;
	Entity * player = getEntityFromWorld(game->world, 0);

	float dis = Vector3Distance(entity->position, player->position);

	// Fly away if to close to player.
	if (dis < MARESCIALLO_CIRCLE_AT_DIS - 1 || dis >= MARESCIALLO_COME_BACK_AT_DIS) {
		flyToPointMaresciallo(game, entity);
	} else {
		circlePlayerMaresciallo(game, entity);
	}

	updateGunMaresciallo(game, entity);
	updateMissilesMaresciallo(game, entity);

	entityCheckTransformedCollisionModel(entity);
}

void drawMaresciallo(Game * game, Entity * entity) {
	entityDraw(entity);

	// Entity * player = getEntityFromWorld(game->world, 0);
	// DrawLine3D(entity->position, player->position, BLUE);
}
