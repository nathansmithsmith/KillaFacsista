#include "guidedMissile.h"
#include "assets.h"
#include "game.h"

void initGuidedMissile(Entity * entity, Game * game) {
	entity->model = &game->assets.models[GUIDED_MISSILE_ASSET];
	entity->collisionModel = entityCreateCollisionModel(*entity->model);
	entity->transformedCollisionModel = entityCreateCollisionModel(*entity->model);
	setEntityRadius(entity);

	entity->health = 0.125;

	// Allocate data.
	entity->data = KF_MALLOC(sizeof(GuidedMissile));

	if (entity->data == NULL) {
		ALLOCATION_ERROR;
		return;
	}

	GuidedMissile * data = (GuidedMissile*)entity->data;

	data->beenBorn = false; // NOT EVEN BORN YET! HA HA!!!

	data->flyToPoint = (EntityFlyToPointInfo){
		.controller.bangbang.speed = 80,
		.controller.bangbang.stopAt = 0.0,
		.controlType = ENTITY_FLY_TO_POINT_BANG_BANG,
		.rotationSpeed = 10.0,
		.applyRotation = true
	};
}

void closeGuidedMissile(Entity * entity) {
	if (entity->data != NULL)
		KF_FREE(entity->data);

	entityFreeCollisionModel(entity->collisionModel);
	entityFreeCollisionModel(entity->transformedCollisionModel);
}

void updateGuidedMissile(Game * game, Entity * entity) {
	entityUpdateLastValues(entity);

	Entity * player = getEntityFromWorld(game->world, 0);
	GuidedMissile * data = (GuidedMissile*)entity->data;

	// Life begins.
	if (!data->beenBorn) {
		data->beenBorn = true;
		data->birthDay = GetTime();
	}

	entityFlyToPoint(entity, player->position, &data->flyToPoint);

	// boom boom time
	if (Vector3Distance(player->position, entity->position) <= GUIDED_MISSILE_BOOM_BOOM_AT)
		guidedMissileGoBoomBoom(game, entity);

	// Death countdown.
	if (GetTime() - data->birthDay >= GUIDED_MISSILE_DEATH_DAY)
		guidedMissileGoBoomBoom(game, entity);

	entityCheckTransformedCollisionModel(entity);
}

void drawGuidedMissile(Game * game, Entity * entity) {
	entityDraw(entity);
}

void guidedMissileGoBoomBoom(Game * game, Entity * entity) {
	Entity * player = getEntityFromWorld(game->world, 0);

	// Get player distance and get the damage the missile will do.
	float distance = Vector3Distance(player->position, entity->position);
	float damage = GUIDED_MISSILE_DAMAGE / distance;

	// Hurt this mother fucker.
	player->health -= damage;

	// Remove its self from the world. I have thought of doing the same for years ):
	entity->health = 0.0;
}
