#include "gameCommon.h"
#include "entity.h"
#include "bullets.h"

#ifndef SERGENTE_H
#define SERGENTE_H

#define SERGENTE_TARGET_DIS_MIN 5
#define SERGENTE_TARGET_DIS_MAX 100
#define SERGENTE_NEXT_POINT_THRESHOLD 60.0
#define SERGENTE_COME_BACK_DIS 800.0
#define SERGENTE_COME_BACK_PERCENT 0.5
#define SERGENTE_ROTATION_SPEED 20.0

// Gun stuff.
#define SERGENTE_COOL_DOWN 0.5
#define SERGENTE_SHOT_COUNT 50
#define SERGENTE_SPREAD 0.05
#define SERGENTE_DAMAGE 0.001
#define SERGENTE_GUN_TARGETING_SPEED 10.0

typedef struct Sergente {
	EntityFlyToPointInfo flyToPoint;
	Vector3 target;

	double timeSinceLastShot;
	Bullet shots[SERGENTE_SHOT_COUNT];
	Vector3 gunTarget;
} Sergente;

void initSergente(Entity * entity, Game * game);
void closeSergente(Entity * entity);
void updateSergente(Game * game, Entity * entity);
void drawSergente(Game * game, Entity * entity);

// Sets target to random.
void createSergenteTarget(Game * game, Entity * entity);

#endif
