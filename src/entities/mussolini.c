#include "mussolini.h"
#include "assets.h"
#include "game.h"
#include "util.h"
#include "missile.h"
#include "world.h"
#include "entity.h"

const Vector3 mussoliniLaunchPositions[MUSSOLINI_LAUNCH_POSITIONS_COUNT] = {
    (Vector3){-66.047, 30.628, 40.838},
    (Vector3){-66.047, 10.209, 40.838},
    (Vector3){-66.047, -10.209, 40.838},
    (Vector3){-66.047, -30.628, 40.838},
    (Vector3){66.047, 30.628, 40.838},
    (Vector3){66.047, 10.209, 40.838},
    (Vector3){66.047, -10.209, 40.838},
    (Vector3){66.047, -30.628, 40.838}
};

void initMussolini(Entity * entity, Game * game) {
	entity->model = &game->assets.models[MUSSOLINI_ASSET];
	entity->collisionModel = entityCreateCollisionModel(*entity->model);
	entity->transformedCollisionModel = entityCreateCollisionModel(*entity->model);
	setEntityRadius(entity);

	// Allocate data.
	entity->data = KF_MALLOC(sizeof(Mussolini));

	if (entity->data == NULL) {
		ALLOCATION_ERROR;
		return;
	}

	Mussolini * data = (Mussolini*)entity->data;

	data->timeSinceLastMissile = GetTime();
}

void closeMussolini(Entity * entity) {
	if (entity->data != NULL)
		KF_FREE(entity->data);

	entityFreeCollisionModel(entity->collisionModel);
	entityFreeCollisionModel(entity->transformedCollisionModel);
}

void updateMussoliniGuns(Game * game, Entity * entity) {
	double t = GetTime();
	Entity * player = getEntityFromWorld(game->world, 0);
	Mussolini * data = (Mussolini*)entity->data;

	if (t - data->timeSinceLastMissile < MUSSOLINI_MISSILE_DOWN_COOL)
		return;

	// Shoot missile.
	Entity missile = createEntity(ENTITY_MISSILE, game);

	// Set missile position.
	SetRandomSeed(clock());
	Vector3 missileStartPosition = mussoliniLaunchPositions[GetRandomValue(0, MUSSOLINI_LAUNCH_POSITIONS_COUNT)];

	missile.position = Vector3Add(
		entity->position,
		Vector3RotateByQuaternion(missileStartPosition, entity->rotation)
	);

	// Launch this mother fucker!!!
	launchMissileAtTarget(&missile, player->position, 200.0);
	startMissileCountDown(&missile, 4.0);
	scheduleEntityToAdd(&game->world, missile);

	//puts("shoot shoot");

	data->timeSinceLastMissile = t;
}

void updateMussolini(Game * game, Entity * entity) {
	entityUpdateLastValues(entity);

	float t = GetFrameTime();
	Entity * player = getEntityFromWorld(game->world, 0);
	Mussolini * data = (Mussolini*)entity->data;

	updateMussoliniGuns(game, entity);

	// Look at player.
	Matrix matrix = MatrixLookAt(player->position, entity->position, (Vector3){0.0, 1.0, 0.0});
	Quaternion rotation = QuaternionInvert(QuaternionFromMatrix(matrix));
	entity->rotation = QuaternionSlerp(entity->rotation, rotation, t * MUSSOLINI_TURN_SPEED);

	entityCheckTransformedCollisionModel(entity);
}

void drawMussolini(Game * game, Entity * entity) {
	entityDraw(entity);
}
