#include "gameCommon.h"
#include "entity.h"

#ifndef GENERALE_H
#define GENERALE_H

#define GENERALE_ZIGZAG_SIZE_MIN 5.0
#define GENERALE_ZIGZAG_SIZE_MAX 100.0
#define GENERALE_NEXT_POINT_THRESHOLD 60.0

#define GENERALE_LASER_TRACKING_SPEED 10.0
#define GENERALE_LASER_MAX_DISTANCE 200.0
#define GENERALE_LASER_THICKNESS 0.25
#define GENERALE_LASER_SIDES 8
#define GENERALE_LASER_DAMAGE 1.0

typedef enum GeneraleZigZag {
	GENERALE_ZIG,
	GENERALE_ZAG
} GeneraleZigZag;

typedef struct Generale {
	EntityFlyToPointInfo flyToPoint;
	GeneraleZigZag zigzag;
	Vector3 target;
	bool targetNotSet;

	Vector3 laserDirection;
	bool isLaserOn;
} Generale;

void initGenerale(Entity * entity, Game * game);
void closeGenerale(Entity * entity);
void updateGenerale(Game * game, Entity * entity);
void drawGenerale(Game * game, Entity * entity);

void getTargetGenerale(Game * game, Entity * entity);

#endif
