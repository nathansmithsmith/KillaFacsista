#include "gameCommon.h"
#include "entity.h"
#include "PID.h"
#include <raylib.h>

#ifndef CAPORALE_H
#define CAPORALE_H

#define CAPORALE_COOLDOWN 1.0
#define CAPORALE_BULLET_DAMAGE 0.01
#define CAPORALE_GUN_TARGETING_SPEED 20.0
#define CAPORALE_LOW_HEALTH_THRESHOLD 0.9
#define CAPORALE_LOW_HEALTH_BULLET_DAMAGE 0.02

typedef struct Caporale {
	EntityFlyToPointInfo flyToPlayer;
	double timeSinceLastShot;

	Vector3 gunTarget;
} Caporale;

void initCaporale(Entity * entity, Game * game);
void closeCaporale(Entity * entity);
void updateCaporale(Game * game, Entity * entity);
void drawCaporale(Game * game, Entity * entity);

#endif
