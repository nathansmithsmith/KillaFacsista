#include "missile.h"
#include "game.h"
#include "assets.h"

// I luv deadly weapons (:

void initMissile(Entity * entity, Game * game) {
    entity->model = &game->assets.models[MISSILE_ASSET];
	entity->collisionModel = entityCreateCollisionModel(*entity->model);
	entity->transformedCollisionModel = entityCreateCollisionModel(*entity->model);
	setEntityRadius(entity);

    entity->health = 0.125;

    entity->data = KF_MALLOC(sizeof(Missile));

    if (entity->data == NULL) {
        ALLOCATION_ERROR;
        return;
    }

    Missile * data = (Missile*)entity->data;

    data->timeToLive = MISSILE_DEFAULT_TIME_TO_LIVE;
    data->birthDay = GetTime();
    data->damage = MISSILE_DEFAULT_DAMAGE;
    data->boomBoomAt = MISSILE_DEFAULT_BOOM_BOOM_AT;

    entity->velocity.speed = MISSILE_DEFAULT_SPEED;
}

void closeMissile(Entity * entity) {
    if (entity->data != NULL)
		KF_FREE(entity->data);

	entityFreeCollisionModel(entity->collisionModel);
	entityFreeCollisionModel(entity->transformedCollisionModel);
}

void updateMissile(Game * game, Entity * entity) {
    entityUpdateLastValues(entity);

    Missile * data = (Missile*)entity->data;
    Entity * player = getEntityFromWorld(game->world, 0);

    entityUpdatePosition(entity);

    // Check the count death to death.
    if (data->timeToLive != MISSILE_LIVE_FOREVER)
        if (GetTime() - data->birthDay >= data->timeToLive) {
            missileGoBoomBoom(game, entity);
            //puts("counted downa downasdjkfdjskl down");
        }

    // We are quite close to the player indeed.
    if (Vector3Distance(player->position, entity->position) <= data->boomBoomAt) {
        missileGoBoomBoom(game, entity);
        //puts("you were fucking hit by this big ass missile");
    }

    entityCheckTransformedCollisionModel(entity);
}

void drawMissile(Game * game, Entity * entity) {
    entityDraw(entity);
}

void launchMissileAtTarget(Entity * entity, Vector3 target, float speed) {
    entity->velocity.speed = speed;
    setMissileDirection(entity, Vector3Normalize(Vector3Subtract(entity->position, target)));
}

// Yo Maggie, If you are reading this I want you to know I have a crush on you (:
// You are a good person to talk to (even though it's mostly me info dumping) and I like your smile.
// Don't worry if you hurt my feelings ever.
// I am not in the best head space most of the time but I can take a lot of punches before hitting the ground.
// Anyone else reading this: GO FUCK YOURSELF YOU MAGA TRUMP WALMART SHOPPPER!!!

void setMissileDirection(Entity * entity, Vector3 direction) {
    // Get look at matrix and rotation.
    Matrix matrix = MatrixLookAt(Vector3Zero(), direction, (Vector3){0.0, 1.0, 0.0});
	entity->rotation = QuaternionInvert(QuaternionFromMatrix(matrix));

    // Set velocity.
    entity->velocity.velocity = (Vector3){
        matrix.m2 * entity->velocity.speed,
		matrix.m6 * entity->velocity.speed,
		matrix.m10 * entity->velocity.speed
    };
}

void setMissileDamage(Entity * entity, float damage) {
    ((Missile*)entity->data)->damage = damage;
}

void setMissileBoomBoomAt(Entity * entity, float boomBoomAt) {
    ((Missile*)entity->data)->boomBoomAt = boomBoomAt;
}

void startMissileCountDown(Entity * entity, double timeToLive) {
    Missile * data = (Missile*)entity->data;
    data->timeToLive = timeToLive;
    data->birthDay = GetTime();
}

// Fucking boom boom time!!!!
void missileGoBoomBoom(Game * game, Entity * entity) {
    Missile * data = (Missile*)entity->data;
    Entity * player = getEntityFromWorld(game->world, 0);

    // Do some damage to the player.
    float distance = Vector3Distance(player->position, entity->position);
    float damage = data->damage / distance;
    player->health -= damage;

    entity->health = 0.0;

    //printf("This old fuck did %f damage at %f distance\n", damage, distance);
}
