#include "gameCommon.h"
#include "entity.h"
#include "world.h"
#include <raylib.h>

#ifndef BULLET_H
#define BULLET_H

// Just a simple, deadly, bullet.
typedef struct Bullet {
	Ray ray;
	EntityId fromId;
	EntityFingerprint fromFingerprint;
	float damage;
} Bullet;

typedef struct BulletHitInfo {
	bool hit;
	bool killed;
	EntityId hitId;
	EntityFingerprint hitFingerprint;
} BulletHitInfo;

// Uses entity postion and direction to create bullet ray.
Bullet createBulletFromEntity(Entity entity, float damage);

// Uses direction argument for direction indeed.
Bullet createBulletFromDirection(Entity entity, Vector3 direction, float damage);

// Shoot this fucker.
BulletHitInfo shootBullet(World * world, Bullet bullet);

// Shott this fucker but only at one entity.
BulletHitInfo shootBulletAtEntity(Entity * entity, Bullet bullet);

#endif
