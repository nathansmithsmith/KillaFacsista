#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <ctype.h>
#include <stdbool.h>
#include <limits.h>
#include <float.h>

#include <raylib.h>
#include <raymath.h>
#include <rlgl.h>
#include "raygui.h"

//#define PLATFORM_WEB

#if defined(PLATFORM_WEB)
    #include <emscripten/emscripten.h>
#endif

#ifndef GAME_COMMON_H
#define GAME_COMMON_H

// Types be like.
typedef struct Game Game;
typedef struct World World;
typedef struct Entity Entity;

// How far from center you can go.
#define GAME_BOUNDS 1000.0

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720

// Memory management.
#define KF_MALLOC(size) malloc(size)
#define KF_CALLOC(nmemb, size) calloc(nmemb, size)
#define KF_REALLOC(ptr, size) realloc(ptr, size)
#define KF_REALLOCARRAY(ptr, nmemb, size) reallocarray(ptr, nmemb, size)
//#define KF_REALLOCARRAY(ptr, nmemb, size) realloc(ptr, nmemb * size)
#define KF_FREE(ptr) free(ptr)

#define ALLOCATION_ERROR TraceLog(LOG_ERROR, "Allocation error in %s:%d", __FILE__, __LINE__)

typedef Vector3 Triangle3D[3];

// Errors.
typedef enum KfError {
	KFERROR = -1,
	KFSUCCESS = 0
} KfError;

#endif
