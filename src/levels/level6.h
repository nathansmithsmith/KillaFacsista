#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL6_H
#define LEVEL6_H

typedef struct Level6 {
    int stage;
} Level6;

void initLevel6(Game * game, Levels * levels);
void closelevel6(Levels * levels);
bool updateLevel6(Game * game, Levels * levels);

#endif
