#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL1_H
#define LEVEL1_H

typedef struct Level1 {
    int stage;
} Level1;

void initLevel1(Game * game, Levels * levels);
void closelevel1(Levels * levels);
bool updateLevel1(Game * game, Levels * levels);

#endif
