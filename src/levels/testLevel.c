#include "testLevel.h"
#include "game.h"
#include "world.h"

void initTestLevel(Game * game, Levels * levels) {
	int i;

	WorldEntry entries[21] = {
		(WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()}
	};

	for (i = 1; i < 21; ++i) {
		entries[i] = (WorldEntry){ENTITY_SOLDATO, (Vector3){0.0, 10.0, i * 10.0 + 50.0}, QuaternionIdentity()};
	}

    addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);
}

void closeTestLevel(Levels * levels) {
}

bool updateTestLevel(Game * game, Levels * levels) {
    return false;
}

