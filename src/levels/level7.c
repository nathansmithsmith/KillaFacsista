#include "level7.h"
#include "game.h"
#include "entityGrouping.h"
#include "entity.h"
#include "world.h"

void initLevel7(Game * game, Levels * levels) {
    WorldEntry entries[] = {
		(WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()},
		(WorldEntry){ENTITY_MARESCIALLO, (Vector3){800.0, 0.0, 0.0}, QuaternionIdentity()},
		(WorldEntry){ENTITY_MARESCIALLO, (Vector3){-2000.0, 0.0, 0.0}, QuaternionIdentity()},
		(WorldEntry){ENTITY_MARESCIALLO, (Vector3){0.0, 2000.0, 0.0}, QuaternionIdentity()},
		(WorldEntry){ENTITY_MARESCIALLO, (Vector3){0.0, -800.0, 0.0}, QuaternionIdentity()}
	};

    addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);
}

void closelevel7(Levels * levels) {
}

bool updateLevel7(Game * game, Levels * levels) {
    return game->world.entitiesCount == 1;
}
