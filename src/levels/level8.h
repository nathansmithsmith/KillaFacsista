#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL8_H
#define LEVEL8_H

typedef struct Level8 {
    int stage;
} Level8;

void initLevel8(Game * game, Levels * levels);
void closelevel8(Levels * levels);
bool updateLevel8(Game * game, Levels * levels);

#endif
