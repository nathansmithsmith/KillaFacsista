#include "bossLevel.h"
#include "game.h"
#include "entity.h"
#include "world.h"
#include "entityGrouping.h"

// The b in boss stands for boobs and the ss is from your moms big sexy ass.

void initBossLevel(Game * game, Levels * levels) {
    WorldEntry entries[] = {
        (WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()},
        (WorldEntry){ENTITY_MUSSOLINI, (Vector3){0.0, 0.0, 1000.0}, QuaternionIdentity()}
    };

    addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);
}

void closeBossLevel(Levels * levels) {
}

bool updateBossLevel(Game * game, Levels * levels) {
    return game->world.entitiesCount == 1;
}
