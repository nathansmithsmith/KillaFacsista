#include "level11.h"
#include "game.h"
#include "world.h"
#include "entity.h"
#include "entityGrouping.h"

void initLevel11(Game * game, Levels * levels) {
    int i;
    const EntityType possibleTypes[] = {ENTITY_SOLDATO, ENTITY_SOLDATO, ENTITY_SOLDATO, ENTITY_CAPORALE, ENTITY_SERGENTE, ENTITY_MARESCIALLO, ENTITY_GENERALE};
    size_t possibleTypesSize = sizeof(possibleTypes) / sizeof(EntityType);

    addEntryToWorld(&game->world, game, (WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()});

    // Add lots of random entities to world.
    // This level is ment to be fucking annoying hehehe.
    SetRandomSeed(time(NULL));
    int randomEntityCount = GetRandomValue(LEVEL11_MIN_ENTITY_COUNT, LEVEL11_MAX_ENTITY_COUNT);
    WorldEntry entries[randomEntityCount];

    for (i = 0; i < randomEntityCount; ++i) {

        // Get random direction then push out to a random distance for a position.
        Vector3 position = (Vector3){
            (float)GetRandomValue(-UCHAR_MAX, UCHAR_MAX) / UCHAR_MAX,
			(float)GetRandomValue(-UCHAR_MAX, UCHAR_MAX) / UCHAR_MAX,
			(float)GetRandomValue(-UCHAR_MAX, UCHAR_MAX) / UCHAR_MAX
        };

        float distance = GetRandomValue(LEVEL11_MIN_ENTITY_DISTANCE, LEVEL11_MAX_ENTITY_DISTANCE);
        distance = (GetRandomValue(0, 1) == 0) ? distance : -distance;

        position = Vector3Scale(position, distance);

        // Add entry.
        entries[i] = (WorldEntry){
            possibleTypes[GetRandomValue(0, possibleTypesSize - 1)],
            position,
            QuaternionIdentity()
        };
    }

    addEntriesToWorld(
        &game->world,
        game,
        entries,
        sizeof(entries) / sizeof(WorldEntry)
    );
}

void closelevel11(Levels * levels) {
}

bool updateLevel11(Game * game, Levels * levels) {
    return game->world.entitiesCount == 1;
}
