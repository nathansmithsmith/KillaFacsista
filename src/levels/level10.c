#include "level10.h"
#include "game.h"
#include "world.h"
#include "entity.h"
#include "entityGrouping.h"

void initLevel10(Game * game, Levels * levels) {
    levels->data = KF_MALLOC(sizeof(Level10));

    if (levels->data == NULL) {
        ALLOCATION_ERROR;
        return;
    }

    Level10 * data = (Level10*)levels->data;
    data->stage = 0;

    addEntryToWorld(&game->world, game, (WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()});

    addSoldatoGroupWithLeader(game, ENTITY_MARESCIALLO, 10, (Vector3){-1000.0, 0.0, 0.0}, (Vector3){0.0, -2.0, -2.0});
    addSoldatoGroupWithLeader(game, ENTITY_CAPORALE, 10, (Vector3){1000.0, 0.0, 0.0}, (Vector3){0.0, 15.0, 15.0});
    addSoldatoGroupWithLeader(game, ENTITY_CAPORALE, 10, (Vector3){1000.0, -1000.0, 0.0}, (Vector3){0.0, 15.0, 15.0});
}

void closelevel10(Levels * levels) {
    if (levels->data != NULL)
        KF_FREE(levels->data);
}

bool updateLevel10(Game * game, Levels * levels) {
   Level10 * data = (Level10*)levels->data;
   Vector3 playerPosition = getEntityFromWorld(game->world, 0)->position;
   bool levelDone = false;

    switch (data->stage) {
        case 0:
            if (game->world.entitiesCount == 1) {
                WorldEntry entries[] = {
                    (WorldEntry){ENTITY_GENERALE,
                        Vector3Add(playerPosition, (Vector3){500.0, 500.0, 500.0}), QuaternionIdentity()},
                    (WorldEntry){ENTITY_GENERALE,
                        Vector3Add(playerPosition, (Vector3){-500.0, -500.0, -500.0}), QuaternionIdentity()},
                    (WorldEntry){ENTITY_GENERALE,
                        Vector3Add(playerPosition, (Vector3){500.0, 0.0, 0.0}), QuaternionIdentity()}
                };

                addEntriesToWorld(
                    &game->world,
                    game,
                    entries,
                    sizeof(entries) / sizeof(WorldEntry)
                );

                data->stage = 1;
            }

            break;
        case 1:
            if (game->world.entitiesCount == 1) {
                addEntityGroupToWorld(
                    game,
                    ENTITY_GENERALE,
                    15,
                    Vector3Add(playerPosition, (Vector3){0.0, 0.0, 900.0}),
                    (Vector3){50.0, 50.0, 50.0}
                );

                data->stage = 2;
            }

            break;
        case 2:
            if (game->world.entitiesCount == 1) {
                levelDone = true;
            }

            break;
        default:
            levelDone = true;
            break;
    }

   return levelDone;
}
