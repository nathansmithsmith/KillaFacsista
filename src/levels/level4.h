#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL4_H
#define LEVEL4_H

void initLevel4(Game * game, Levels * levels);
void closelevel4(Levels * levels);
bool updateLevel4(Game * game, Levels * levels);

#endif
