#include "gameCommon.h"
#include "levels.h"

#ifndef BOSS_LEVEL_H
#define BOSS_LEVEL_H

void initBossLevel(Game * game, Levels * levels);
void closeBossLevel(Levels * levels);
bool updateBossLevel(Game * game, Levels * levels);

#endif
