#include "level1.h"
#include "game.h"
#include "world.h"
#include "entity.h"
#include "entitiesInclude.h"
#include "entityGrouping.h"

void initLevel1(Game * game, Levels * levels) {
    int i;

	levels->data = KF_MALLOC(sizeof(Level1));

	if (levels->data == NULL) {
		ALLOCATION_ERROR;
		return;
	}

	Level1 * data = (Level1*)levels->data;
	data->stage = 0;

	WorldEntry entries[] = {
		(WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()},
		(WorldEntry){ENTITY_SOLDATO, (Vector3){0.0, 10.0, 800.0}, QuaternionIdentity()}
	};

    addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);
}

void closelevel1(Levels * levels) {
	if (levels->data != NULL)
		KF_FREE(levels->data);
}

bool updateLevel1(Game * game, Levels * levels) {
	int i;
	Level1 * data = (Level1*)levels->data;
	Entity * player = getEntityFromWorld(game->world, 0);
	bool levelDone = false;

	switch (data->stage) {
		case 0:
			// Next stage
			if (game->world.entitiesCount == 1) {
				Vector3 position = Vector3Add(player->position, (Vector3){0.0, 0.0, 1000.0});
				Vector3 spacing = (Vector3){0.0, 10.0, 10.0};
				addEntityGroupToWorld(game, ENTITY_SOLDATO, 10, position, spacing);

				data->stage = 1;
			}

			break;
		case 1:
			if (game->world.entitiesCount == 1)
				levelDone = true;

			break;
		default:
			levelDone = true;
			break;
	}

    return levelDone;
}
