#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL5_H
#define LEVEL5_H

typedef struct Level5 {
    int stage;
} Level5;

void initLevel5(Game * game, Levels * levels);
void closelevel5(Levels * levels);
bool updateLevel5(Game * game, Levels * levels);

#endif
