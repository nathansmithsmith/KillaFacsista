#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL2_H
#define LEVEL2_H

void initLevel2(Game * game, Levels * levels);
void closelevel2(Levels * levels);
bool updateLevel2(Game * game, Levels * levels);

#endif
