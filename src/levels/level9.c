#include "level9.h"
#include "game.h"
#include "world.h"
#include "entity.h"
#include "entityGrouping.h"

void initLevel9(Game * game, Levels * levels) {
    WorldEntry entries[] = {
		(WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()},
		(WorldEntry){ENTITY_GENERALE, (Vector3){0.0, 0.0, -800.0}, QuaternionIdentity()},
		(WorldEntry){ENTITY_GENERALE, (Vector3){-800.0, -800.0, -800.0}, QuaternionIdentity()}
	 };

    addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);

    addSoldatoGroupWithLeader(game, ENTITY_CAPORALE, 3, (Vector3){-1000.0, 0.0, 0.0}, (Vector3){0.0, -15.0, -15.0});
    addSoldatoGroupWithLeader(game, ENTITY_CAPORALE, 3, (Vector3){1000.0, 0.0, 0.0}, (Vector3){0.0, 15.0, 15.0});
}

void closelevel9(Levels * levels) {
}

bool updateLevel9(Game * game, Levels * levels) {
    return game->world.entitiesCount == 1;
}
