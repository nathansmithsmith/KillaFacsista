#include "level4.h"
#include "game.h"
#include "world.h"
#include "entity.h"
#include "entityGrouping.h"

void initLevel4(Game * game, Levels * levels) {
    int i;

    // Add player.
    addEntryToWorld(&game->world, game, (WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()});

    Vector3 positions[] = {
        (Vector3){800.0, 0.0, 0.0},
        (Vector3){-800.0, 0.0, 0.0},
        (Vector3){0.0, 800.0, 0.0},
        (Vector3){0.0, -800.0, 0.0},
        (Vector3){0.0, 0.0, 800.0},
        (Vector3){0.0, 0.0, -800.0}
    };

    Vector3 spacing = (Vector3){0.0, 15.0, 15.0};

    // Add groups.
    for (i = 0; i < sizeof(positions) / sizeof(Vector3); ++i) {
        addSoldatoGroupWithLeader(game, ENTITY_CAPORALE, 3, positions[i], spacing);
        spacing = Vector3Negate(spacing); // Swap group direction each time.
    }
}

void closelevel4(Levels * levels) {
}

bool updateLevel4(Game * game, Levels * levels) {
    return game->world.entitiesCount == 1;
}
