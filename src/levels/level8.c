#include "level8.h"
#include "game.h"
#include "world.h"
#include "entity.h"
#include "entityGrouping.h"

void initLevel8(Game * game, Levels * levels) {
	levels->data = KF_MALLOC(sizeof(Level8));

	if (levels->data == NULL) {
		ALLOCATION_ERROR;
		return;
	}

	Level8 * data = (Level8*)levels->data;
	data->stage = 0;

     WorldEntry entries[] = {
		(WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()},
		(WorldEntry){ENTITY_GENERALE, (Vector3){0.0, 0.0, 700.0}, QuaternionIdentity()}
	 };

    addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);
}

void closelevel8(Levels * levels) {
	if (levels->data != NULL)
		KF_FREE(levels->data);
}

bool updateLevel8(Game * game, Levels * levels) {
	Level8 * data = (Level8*)levels->data;
	bool levelDone = false;

	switch (data->stage) {
		case 0:
			if (game->world.entitiesCount == 1) {
				Vector3 playerPosition = getEntityFromWorld(game->world, 0)->position;

				WorldEntry entries[] = {
					(WorldEntry){ENTITY_GENERALE, Vector3Add(playerPosition, (Vector3){0.0, 0.0, -1500.0}),
						QuaternionIdentity()},
					(WorldEntry){ENTITY_GENERALE, Vector3Add(playerPosition, (Vector3){-1000.0, 0.0, -500.0}),
						QuaternionIdentity()},
					(WorldEntry){ENTITY_GENERALE, Vector3Add(playerPosition, (Vector3){0.0, -500.0, 1500.0}),
						QuaternionIdentity()},
					(WorldEntry){ENTITY_MARESCIALLO, Vector3Add(playerPosition, (Vector3){-1500.0, 0.0, 1500.0}),
						QuaternionIdentity()}
				};

				addEntriesToWorld(
					&game->world,
					game,
					entries,
					sizeof(entries) / sizeof(WorldEntry)
				);

				data->stage = 1;
			}

			break;
		case 1:
			if (game->world.entitiesCount == 1) {
				levelDone = true;
			}

			break;
		default:
			levelDone = true;
			break;
	}

	return levelDone;
}
