#include "level3.h"
#include "game.h"
#include "world.h"
#include "entityGrouping.h"

void initLevel3(Game * game, Levels * levels) {
	levels->data = KF_MALLOC((sizeof(Level3)));

	if (levels->data == NULL) {
		ALLOCATION_ERROR;
		return;
	}

	Level3 * data = (Level3*)levels->data;
	data->stage = 0;

    WorldEntry entries[] = {
		(WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()},
		(WorldEntry){ENTITY_CAPORALE, (Vector3){0.0, 0.0, 300.0}, QuaternionIdentity()}
	};

    addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);
}

void closelevel3(Levels * levels) {
	if (levels->data != NULL)
		KF_FREE(levels->data);
}

bool updateLevel3(Game * game, Levels * levels) {
	int i;
	Level3 * data = (Level3*)levels->data;
	bool levelDone = false;

	switch (data->stage) {
		case 0:
			if (game->world.entitiesCount == 1) {
				Vector3 playerPosition = getEntityFromWorld(game->world, 0)->position;
				Vector3 spacing = (Vector3){0.0, 15.0, 15.0};

				addSoldatoGroupWithLeader(game, ENTITY_CAPORALE, 3, Vector3Add((Vector3){0.0, 0.0, 800.0}, playerPosition), spacing);
				spacing = Vector3Negate(spacing);
				addSoldatoGroupWithLeader(game, ENTITY_CAPORALE, 3, Vector3Add((Vector3){0.0, 0.0, -800.0}, playerPosition), spacing);

				data->stage = 1;
			}

			break;
		case 1:
			if (game->world.entitiesCount == 1) {
				levelDone = true;
			}

			break;
		default:
			levelDone = true;
			break;
	}

    return levelDone;
}
