#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL10_H
#define LEVEL10_H

typedef struct Level10 {
    int stage;
} Level10;

void initLevel10(Game * game, Levels * levels);
void closelevel10(Levels * levels);
bool updateLevel10(Game * game, Levels * levels);

#endif
