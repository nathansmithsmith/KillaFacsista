#include "level2.h"
#include "game.h"
#include "world.h"
#include "entity.h"
#include "entitiesInclude.h"
#include "entityGrouping.h"

void initLevel2(Game * game, Levels * levels) {
    int i;

	WorldEntry entries[] = {
		(WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()}
	};

    addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);

    Vector3 groups[] = {
        (Vector3){0.0, 0.0, 1000.0},
        (Vector3){0.0, 0.0, -1000.0},
        (Vector3){1000.0, 0.0, 0.0},
        (Vector3){-1000.0, 0.0, 0.0},
        (Vector3){0.0, 1000.0, 0.0},
        (Vector3){0.0, -1000.0, 0.0}
    };

   for (i = 0; i < sizeof(groups) / sizeof(Vector3); ++i) {
       Vector3 spacing = (Vector3){0.0, 10.0, 10.0};
       addEntityGroupToWorld(game, ENTITY_SOLDATO, 7, groups[i], spacing);
    }
}

void closelevel2(Levels * levels) {
}

bool updateLevel2(Game * game, Levels * levels) {
	if (game->world.entitiesCount == 1)
		return true;

    return false;
}

