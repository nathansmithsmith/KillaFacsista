#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL11_H
#define LEVEL11_H

#define LEVEL11_MIN_ENTITY_COUNT 15
#define LEVEL11_MAX_ENTITY_COUNT 100

#define LEVEL11_MIN_ENTITY_DISTANCE 500.0
#define LEVEL11_MAX_ENTITY_DISTANCE 2500.0

void initLevel11(Game * game, Levels * levels);
void closelevel11(Levels * levels);
bool updateLevel11(Game * game, Levels * levels);

#endif
