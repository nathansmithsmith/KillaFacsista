#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL9_H
#define LEVEL9_H

void initLevel9(Game * game, Levels * levels);
void closelevel9(Levels * levels);
bool updateLevel9(Game * game, Levels * levels);

#endif
