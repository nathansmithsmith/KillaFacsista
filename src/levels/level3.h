#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL3_H
#define LEVEL3_H

typedef struct Level3 {
    int stage;
} Level3;

void initLevel3(Game * game, Levels * levels);
void closelevel3(Levels * levels);
bool updateLevel3(Game * game, Levels * levels);

#endif
