#include "gameCommon.h"
#include "levels.h"

#ifndef TEST_LEVEL_H
#define TEST_LEVEL_H

void initTestLevel(Game * game, Levels * levels);
void closeTestLevel(Levels * levels);
bool updateTestLevel(Game * game, Levels * levels);

#endif
