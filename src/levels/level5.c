#include "level5.h"
#include "game.h"
#include "world.h"
#include "entityGrouping.h"

void initLevel5(Game * game, Levels * levels) {
    levels->data = KF_MALLOC(sizeof(Level5));

    if (levels->data == NULL) {
        ALLOCATION_ERROR;
        return;
    }

    Level5 * data = (Level5*)levels->data;
    data->stage = 0;

     WorldEntry entries[] = {
		(WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()},
		(WorldEntry){ENTITY_SERGENTE, (Vector3){0.0, 0.0, 300.0}, QuaternionIdentity()}
	};

    addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);
}

void closelevel5(Levels * levels) {
    if (levels->data != NULL)
		KF_FREE(levels->data);
}

bool updateLevel5(Game * game, Levels * levels) {
    Level5 * data = (Level5*)levels->data;
    bool levelDone = false;
    Entity * player = getEntityFromWorld(game->world, 0);
    Vector3 playerPosition = player->position;

    switch (data->stage) {
        case 0:
            if (game->world.entitiesCount == 1) {
                WorldEntry entries[] = {
                    (WorldEntry){ENTITY_SERGENTE,
                        Vector3Add((Vector3){0.0, 0.0, 800.0}, playerPosition), QuaternionIdentity()},
                    (WorldEntry){ENTITY_SERGENTE,
                        Vector3Add((Vector3){0.0, 0.0, -800.0}, playerPosition), QuaternionIdentity()},
                    (WorldEntry){ENTITY_SERGENTE,
                        Vector3Add((Vector3){0.0, 800.0, 0.0}, playerPosition), QuaternionIdentity()},
                    (WorldEntry){ENTITY_SERGENTE,
                        Vector3Add((Vector3){0.0, -800.0, 0.0}, playerPosition), QuaternionIdentity()}
                };

                addEntriesToWorld(
                    &game->world,
                    game,
                    entries,
                    sizeof(entries) / sizeof(WorldEntry)
                );

                data->stage = 1;
            }

            break;
        case 1:
            if (game->world.entitiesCount == 1) {
                float distanceFromPlayer = 1500.0;

                addEntityGroupToWorld(
                    game,
                    ENTITY_SOLDATO,
                    10,
                    Vector3Add(
                        Vector3Scale(
                            Vector3RotateByQuaternion((Vector3){0.5, 0.0, 1.0}, player->rotation),
                                     distanceFromPlayer
                        ),
                        playerPosition
                    ),
                    (Vector3){0.0, 10.0, 10.0}
                );

                data->stage = 2;
            }

            break;
        case 2:
            if (game->world.entitiesCount == 1)
                levelDone = true;

            break;
        default:
            levelDone = true;
            break;
    }

    return levelDone;
}
