#include "level6.h"
#include "game.h"
#include "world.h"
#include "entity.h"
#include "entityGrouping.h"

void initLevel6(Game * game, Levels * levels) {
    levels->data = KF_MALLOC(sizeof(Level6));

    if (levels->data == NULL) {
        ALLOCATION_ERROR;
        return;
    }

    Level6 * data = (Level6*)levels->data;
    data->stage = 0;

    // Add player.
    WorldEntry entries[] = {
        (WorldEntry){ENTITY_ANTIFA, (Vector3){0.0, 0.0, 0.0}, QuaternionIdentity()},
        (WorldEntry){ENTITY_MARESCIALLO, (Vector3){0.0, 0.0, 1000.0}, QuaternionIdentity()}
    };

    addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);
}

void closelevel6(Levels * levels) {
    if (levels->data != NULL)
		KF_FREE(levels->data);
}

bool updateLevel6(Game * game, Levels * levels) {
    Level6 * data = (Level6*)levels->data;
    bool levelDone = false;

    switch (data->stage) {
        case 0:
            if (game->world.entitiesCount == 1) {
                Vector3 playerPosition = getEntityFromWorld(game->world, 0)->position;

                WorldEntry entries[] = {
                    (WorldEntry){ENTITY_SERGENTE,
                        Vector3Add((Vector3){400.0, 400.0, 400.0}, playerPosition), QuaternionIdentity()},
                    (WorldEntry){ENTITY_SERGENTE,
                        Vector3Add((Vector3){-400.0, -400.0, -400.0}, playerPosition), QuaternionIdentity()}
                };

                addEntriesToWorld(
                    &game->world,
                    game,
                    entries,
                    sizeof(entries) / sizeof(WorldEntry)
                );

                data->stage = 1;
            }

            break;
        case 1:
            if (game->world.entitiesCount == 1)
                levelDone = true;

            break;
        default:
            levelDone = true;
            break;
    }

    return levelDone;
}
