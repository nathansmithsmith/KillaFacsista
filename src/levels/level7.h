#include "gameCommon.h"
#include "levels.h"

#ifndef LEVEL7_H
#define LEVEL7_H

void initLevel7(Game * game, Levels * levels);
void closelevel7(Levels * levels);
bool updateLevel7(Game * game, Levels * levels);

#endif
