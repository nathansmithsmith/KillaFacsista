#include "gameCommon.h"

#ifndef GYROSCOPE_H
#define GYROSCOPE_H

#define GYROSCOPE_LINE_THICKNESS 2.0
#define GYROSCOPE_TEXTURE_SIZE 80
#define GYROSCOPE_CAMERA_DISTANCE 3.0
#define GYROSCOPE_TEXTURE_COLOR (Color){0xff, 0xff, 0xff, 0xcd}

// The gyroscope is rended in 3d but not in the main scene.
// We use a render texture to get the gyroscope render onto the screen.
typedef struct Gyroscope {
    Vector2 position;
    Camera3D camera;
    RenderTexture texture;
} Gyroscope;

void initGyroscope(Gyroscope * gyroscope);
void closeGyroscope(Gyroscope * gyroscope);

void resetGyroscopePosition(Gyroscope * gyroscope);

void drawGyroscope(Game * game, Gyroscope * gyroscope);

#endif
