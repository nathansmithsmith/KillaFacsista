#include "gameCommon.h"
#include "entity.h"
#include <raylib.h>

#ifndef WORLD_H
#define WORLD_H

#define FINGERPRINT_START_RANGE_MIN 0xf
#define FINGERPRINT_START_RANGE_MAX 0xff

typedef struct World {
	Entity * entities;
	size_t entitiesCount;

	// For getting entity id;
	EntityId * lookUp;
	size_t lookUpSize;

	EntityId * vacantIds;
	size_t vacantIdsCount;

	Entity * entitiesToAdd;
	size_t entitiesToAddCount;

	// Used for making fingerprints for entities.
	EntityFingerprint currentFingerprint;
} World;

void initWorld(World * world);
void freeWorld(World * world);

#define ENTITY_ID_NONE -1

// ENTITY_ID_NONE on error.
Entity * getEntityFromWorld(World world, EntityId id);
EntityId addEntityToWorld(World * world, Entity entity);
KfError removeEntityFromWorld(World * world, EntityId id);

// Entities can't be added to world during update loop so we schedule the mother fuckers.
KfError scheduleEntityToAdd(World * world, Entity entity);
KfError handleScheduledEntities(World * world);

void updateWorld(World * world, Game * game);
void drawWorld(World * world, Game * game);

// Returns the closest entity that the ray hits or ENTITY_NONE;
// From can be used to stop it from hitting the entity the ray is coming from...
EntityId traceRayToEntityInWorld(World * world, Ray ray, EntityFingerprint from, bool useFrom);

void debugWorld(World * world);

// Used for creating worlds.
typedef struct WorldEntry {
	EntityType type;
	Vector3 position;
	Quaternion rotation;
} WorldEntry;

KfError addEntryToWorld(World * world, Game * game, WorldEntry entry);
KfError addEntriesToWorld(World * world, Game * game, WorldEntry * entries, size_t entriesCount);

#endif
