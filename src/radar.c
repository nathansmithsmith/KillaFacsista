#include "radar.h"
#include "game.h"
#include "entity.h"
#include "world.h"
#include "assets.h"
#include "entitiesInclude.h"

void initRadar(Radar * radar) {
    resetRadarPosition(radar);

    radar->camera = (Camera3D){
        .fovy = 45.0,
        .projection = CAMERA_ORTHOGRAPHIC,
        .target = Vector3Zero(),
        .position = (Vector3){0.0, RADAR_CAMERA_DISTANCE, RADAR_CAMERA_DISTANCE},
        .up = (Vector3){0.0, 1.0, 0.0}
    };

    radar->texture = LoadRenderTexture(RADAR_TEXTURE_SIZE, RADAR_TEXTURE_SIZE);
}

void closeRadar(Radar * radar) {
    UnloadRenderTexture(radar->texture);
}

void resetRadarPosition(Radar * radar) {
    float width = GetScreenWidth();
    float height = GetScreenHeight();

    radar->position = (Vector2){
        width - RADAR_TEXTURE_SIZE,
        height - RADAR_TEXTURE_SIZE
    };
}

void drawRadar3DParts(Game * game, Radar * radar) {
    int i;

    World * world = &game->world;
    Entity * player = getEntityFromWorld(*world, 0);
    AntifaShip * data = (AntifaShip*)player->data;

    BeginTextureMode(radar->texture);
    ClearBackground(RADAR_COLOR);
    BeginMode3D(radar->camera);

    // Get targeted entity if there is one.
    Entity * targetedEntity = NULL;

    if (data->doAutoTarget)
        targetedEntity = getEntityFromWorld(game->world, data->targetedEntityId);

    // Draw entities on radar. Skip player.
    for (i = 1; i < world->entitiesCount; ++i) {
        Entity * entity = &world->entities[i];

        // Too far.
        if (Vector3Distance(player->position, entity->position) > RADAR_MAX_DISTANCE)
            continue;

        Color color = RADAR_ENTITY_COLOR;

        // Is targeted.
        if (targetedEntity != NULL)
            if (entity->fingerprint == targetedEntity->fingerprint)
                color = RADAR_TARGETED_ENTITY_COLOR;

        DrawSphere(
            Vector3Scale(Vector3Subtract(entity->position, player->position), RADAR_WORLD_SCALE),
            RADAR_POINT_SIZE,
            color
        );
    }

    Vector3 direction = Vector3Scale(
        Vector3RotateByQuaternion((Vector3){0.0, 0.0, 1.0}, player->rotation),
        RADAR_POINT_SIZE * 2.0
    );

    // Draw player.
    DrawCylinderWiresEx(
        Vector3Scale(direction, -1.0),
        direction,
        RADAR_POINT_SIZE,
        0.0,
        8,
        RADAR_PLAYER_COLOR
    );

    EndMode3D();
    EndTextureMode();

    // Draw render texture.
    DrawTextureV(radar->texture.texture, radar->position, WHITE);

}

void drawRadarGrid(Game * game, Radar * radar) {
}

void drawRadar(Game * game, Radar * radar) {
    drawRadar3DParts(game, radar);

    // Draw cross.
    float halfWayPoint = RADAR_TEXTURE_SIZE / 2.0;

    // Top to bottom.
    // DrawLine(
    //     radar->position.x + halfWayPoint,
    //     radar->position.y,
    //     radar->position.x + halfWayPoint,
    //     radar->position.y + RADAR_TEXTURE_SIZE,
    //     RADAR_CROSS_COLOR
    // );
    //
    // // Left to right.
    // DrawLine(
    //     radar->position.x,
    //     radar->position.y + halfWayPoint,
    //     radar->position.x + RADAR_TEXTURE_SIZE,
    //     radar->position.y + halfWayPoint,
    //     RADAR_CROSS_COLOR
    // );
}
