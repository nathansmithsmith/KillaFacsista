#include "gameCommon.h"

#ifndef SETTINGS_H
#define SETTINGS_H

// What is the player using for control.
typedef enum ControlMode {
	JOYSTICK_CONTROL,
	KEYBOARD_AND_MOUSE_CONTROL
} ControlMode;

typedef struct Settings {
	// Player control stuff.
	ControlMode controlMode;

	// Keyboard and mouse control.
	float mouseSensitivity;
	float speedMod;
	float scrollBarSpeed;
	bool lockMouse;

	// Joystick control.
	int gamePadNum;
	int pitchStick;
	int yawStick;
	int rollStick;
	int speedStick;
	float joystickSensitivity;

	// Fps shit.
	int fps;
	bool drawFps;

	// World render shit.
	int renderWidth;
	int renderHeight;
	bool useWorldRenderTexture;

	// Preview.
	float previewMouseSensitivity;
	float previewZoomSpeed;
} Settings;

void initSettings(Settings * settings);
void applySettings(Settings * settings);

#endif
