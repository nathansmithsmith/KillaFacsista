#include "bullets.h"
#include "game.h"

Bullet createBulletFromEntity(Entity entity, float damage) {
	return (Bullet){
		.ray = (Ray){
			entity.position,
			Vector3RotateByQuaternion((Vector3){0.0, 0.0, 1.0}, entity.rotation)
		},
		.fromId = entity.id,
		.fromFingerprint = entity.fingerprint,
		.damage = damage
	};
}

Bullet createBulletFromDirection(Entity entity, Vector3 direction, float damage) {
	return (Bullet){
		.ray = (Ray){
			entity.position,
			direction
		},
		.fromId = entity.id,
		.fromFingerprint = entity.fingerprint,
		.damage = damage
	};
}

BulletHitInfo handleBulletHit(Entity * entity, Bullet bullet) {
	// Handle health.
	entity->health -= bullet.damage;
	entity->health = Clamp(entity->health, ENTITY_MIN_HEALTH, ENTITY_MAX_HEALTH);

	// Check hit info.
	BulletHitInfo hitInfo = (BulletHitInfo){
		.hit = true,
		.killed = entity->health == 0.0,
		.hitId = entity->id,
		.hitFingerprint = entity->fingerprint
	};

	return hitInfo;
}

BulletHitInfo shootBullet(World * world, Bullet bullet) {
	EntityId hitId = traceRayToEntityInWorld(world, bullet.ray, bullet.fromFingerprint, true);
	Entity * hitEntity = getEntityFromWorld(*world, hitId);

	// We hit a fucker.
	if (hitEntity != NULL)
		return handleBulletHit(hitEntity, bullet);

	return (BulletHitInfo){
		.hit = false,
		.killed = false,
		.hitId = ENTITY_NONE,
	};
}

BulletHitInfo shootBulletAtEntity(Entity * entity, Bullet bullet) {
	RayCollision collision = traceRayToEntity(*entity, bullet.ray);

	if (collision.hit)
		return handleBulletHit(entity, bullet);

	return (BulletHitInfo){
		.hit = false,
		.killed = false,
		.hitId = ENTITY_NONE,
	};
}
