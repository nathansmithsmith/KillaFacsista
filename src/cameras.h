#include "gameCommon.h"

#ifndef CAMERAS_H
#define CAMERAS_H

#define FIRST_PERSON_CAMERA_DISTANCE 5.0
#define THIRD_PERSON_CAMERA_DISTANCE (Vector3){0.0, 10.0, -20.0}
#define DEBUG_CAMERA_POSITION (Vector3){0.0, 200.0, 100.0}

#define CAMERA_COUNT 4

// Each camera has a game loop callback and a init callback.
typedef void (*CameraInitCb)(Game * game, Camera3D * camera);
typedef void (*CameraCb)(Game * game, Camera3D * camera);

extern const CameraInitCb cameraInitCallbacks[CAMERA_COUNT];
extern const CameraCb cameraCallbacks[CAMERA_COUNT];

typedef enum CameraId {
    FIRST_PERSON_CAMERA,
    THIRD_PERSON_CAMERA,
    ZOOM_CAMERA,
    DEBUG_CAMERA
} CameraId;

// A array of the cameras.
typedef Camera3D Cameras[CAMERA_COUNT];

void initCameras(Game * game, Cameras cameras);
void runCameraUpdate(Game * game, Cameras cameras, CameraId id);

#endif
