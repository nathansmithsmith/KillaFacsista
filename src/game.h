#include "gameCommon.h"
#include "screens/mainMenu.h"
#include "screens/gameScreen.h"
#include "screens/howToPlayScreen.h"
#include "screens/infoScreen.h"
#include "cameras.h"
#include "entity.h"
#include "assets.h"
#include "world.h"
#include "settings.h"
#include "bullets.h"
#include "levels.h"
#include "killLog.h"

#ifndef GAME_H
#define GAME_H

typedef enum ScreenId {
	SCREEN_MAIN_MENU,
	SCREEN_GAME,
	HOW_TO_PLAY_SCREEN,
	INFO_SCREEN
} ScreenId;

typedef struct Game {
	ScreenId screenId;
	MainMenu mainMenu;
	GameScreen gameScreen;
	HowToPlayScreen howToPlayScreen;
	InfoScreen infoScreen;
	Cameras cameras;
	Assets assets;
	World world;
	Levels levels;
	Settings settings;
	KillLog killLog;
} Game;

void initGame(Game * game);
void closeGame(Game * game);
void updateGame(Game * game);

#endif
