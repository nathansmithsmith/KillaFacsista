#include <raylib.h>

#define RAYGUI_IMPLEMENTATION
#include "raygui.h"

#define RRES_IMPLEMENTATION
#include "rres.h"

#define RRES_RAYLIB_IMPLEMENTATION
#include "rres-raylib.h"
