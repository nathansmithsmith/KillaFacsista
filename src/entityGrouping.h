#include "gameCommon.h"
#include "entity.h"
#include <raylib.h>

#ifndef ENTITY_GROUPING_H
#define ENTITY_GROUPING_H

void addEntityGroupToWorld(Game * game, EntityId id, int groupSize, Vector3 position, Vector3 spacing);
void addSoldatoGroupWithLeader(Game * game, EntityId leader, int groupSize, Vector3 position, Vector3 spacing);

#endif
