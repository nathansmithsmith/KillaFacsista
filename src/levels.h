#include "gameCommon.h"
#include "world.h"
#include "entity.h"

#ifndef LEVELS_H
#define LEVELS_H

#define LEVELS_COUNT 13
#define GAME_LEVELS_COMPLETE_AT 11

#define NO_LEVEL -1

typedef struct Levels Levels;

typedef void (*LevelInitCb)(Game * game, Levels * levels);
typedef void (*LevelCloseCb)(Levels * levels);
typedef bool (*LevelUpdateCb)(Game * game, Levels * levels); // Returns true when level is complete

typedef struct Levels {
    int currentLevel;
    void * data; // Null if not used.
} Levels;

typedef struct LevelInfo {
    LevelInitCb initCb;
    LevelCloseCb closeCb;
    LevelUpdateCb updateCb;
} LevelInfo;

extern const LevelInfo levelInfoList[LEVELS_COUNT];

void initLevels(Levels * levels);
void startLevel(Game * game, Levels * levels, int levelNum);
bool updateLevel(Game * game, Levels * levels); // Returns true on end of level
void endLevel(Game * game, Levels * levels);

#endif
