#include "gameCommon.h"

#ifndef PID_H
#define PID_H

// Hehehehe! PID CONTROLLER INDEED!

typedef struct PIDConfig {
	float kP;
	float kI;
	float kD;

	// Angles are fucking weird.
	bool angleMode;
	bool doClamp;
	float min;
	float max;
} PIDConfig;

typedef struct PID {
	float p;
	float i;
	float d;

	float kP;
	float kI;
	float kD;

	bool angleMode;
	bool doClamp;
	float min;
	float max;

	float error;
	float pastError;

	float output;
} PID;

PID createPID(PIDConfig config);
float runPID(float setpoint, float processValue, PID * pid);
void resetPID(PID * pid);

// Angle shit.
float angleDis(float a, float b);

#endif
