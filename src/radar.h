#include "gameCommon.h"

#ifndef RADAR_H
#define RADAR_H

#define RADAR_TEXTURE_SIZE 200
#define RADAR_WORLD_SCALE 0.01
#define RADAR_POINT_SIZE 0.75
#define RADAR_MAX_DISTANCE 2000.0
#define RADAR_CAMERA_DISTANCE 6.0

#define RADAR_COLOR (Color){255, 255, 255, 85}
#define RADAR_CROSS_COLOR (Color){YELLOW.r, YELLOW.g, YELLOW.b, 127}
#define RADAR_ENTITY_COLOR BLACK
#define RADAR_TARGETED_ENTITY_COLOR RED
#define RADAR_PLAYER_COLOR BLUE

typedef struct Radar {
    Vector2 position;
    Camera3D camera;
    RenderTexture texture;
} Radar;

void initRadar(Radar * radar);
void closeRadar(Radar * radar);

void resetRadarPosition(Radar * radar);

void drawRadar(Game * game, Radar * radar);

#endif
