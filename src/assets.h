#include "gameCommon.h"

#ifndef ASSETS_H
#define ASSETS_H

#define ASSET_PATH_MAX 255

#define TEXTURE_ASSET_COUNT 5
#define MODEL_ASSET_COUNT 11

// Paths to assets.
extern const char textureAssetPaths[TEXTURE_ASSET_COUNT][ASSET_PATH_MAX];
extern const char modelAssetPaths[MODEL_ASSET_COUNT][ASSET_PATH_MAX];

typedef int32_t AssetId;

// Texture asset ids.
enum {
	ICON_ASSET,
	ICON128_ASSET,
	ICON64_ASSET,
	GYROSCOPE_TEXTURE_ASSET,
	SKY_TEXTURE_ASSET
};

// Model asset ids.
enum {
	ANTIFA_SHIP_ASSET,
	SOLDATO_ASSET,
	CAPORATE_ASSET,
	SERGENTE_ASSET,
	MARESCIALLO_ASSET,
	GENERALE_ASSET,
	MUSSOLINI_ASSET,
	GUIDED_MISSILE_ASSET,
	MISSILE_ASSET,
	GYROSCOPE_ASSET,
	SKY_ASSET
};

typedef struct Assets {
	Texture2D textures[TEXTURE_ASSET_COUNT];
	Model models[MODEL_ASSET_COUNT];
} Assets;

void LoadAssets(Assets * assets);
void unloadAssets(Assets * assets);

#endif
