#include "levels.h"
#include "game.h"
#include "world.h"
#include "killLog.h"
#include "levelsInclude.h"

const LevelInfo levelInfoList[LEVELS_COUNT] = {
    (LevelInfo){initLevel1, closelevel1, updateLevel1},
    (LevelInfo){initLevel2, closelevel2, updateLevel2},
    (LevelInfo){initLevel3, closelevel3, updateLevel3},
    (LevelInfo){initLevel4, closelevel4, updateLevel4},
    (LevelInfo){initLevel5, closelevel5, updateLevel5},
    (LevelInfo){initLevel6, closelevel6, updateLevel6},
    (LevelInfo){initLevel7, closelevel7, updateLevel7},
    (LevelInfo){initLevel8, closelevel8, updateLevel8},
    (LevelInfo){initLevel9, closelevel9, updateLevel9},
    (LevelInfo){initLevel10, closelevel10, updateLevel10},
    (LevelInfo){initLevel11, closelevel11, updateLevel11},
    (LevelInfo){initBossLevel, closeBossLevel, updateBossLevel},
    (LevelInfo){initTestLevel, closeTestLevel, updateTestLevel}
};

void initLevels(Levels * levels) {
    levels->currentLevel = NO_LEVEL;
    levels->data = NULL;
}

void startLevel(Game * game, Levels * levels, int levelNum) {
    levels->currentLevel = levelNum;
    resetKillLog(&game->killLog);
    initWorld(&game->world);
    levelInfoList[levelNum].initCb(game, levels);
}

bool updateLevel(Game * game, Levels * levels) {
    if (levels->currentLevel != NO_LEVEL)
        return levelInfoList[levels->currentLevel].updateCb(game, levels);

    return false;
}

void endLevel(Game * game, Levels * levels) {
   if (levels->currentLevel != NO_LEVEL)
        levelInfoList[levels->currentLevel].closeCb(levels);

    freeWorld(&game->world);
    levels->currentLevel = NO_LEVEL;
    levels->data = NULL;
}
