#include "entityGrouping.h"
#include "entitiesInclude.h"
#include "game.h"
#include "world.h"

void addEntityGroupToWorld(Game * game, EntityId id, int groupSize, Vector3 position, Vector3 spacing) {
	int i;
	WorldEntry entries[groupSize];

	for (i = 0; i < groupSize; ++i)
		entries[i] = (WorldEntry){
			id,
			Vector3Add(position, Vector3Multiply((Vector3){i, i, i}, spacing)),
			QuaternionIdentity()
		};

	addEntriesToWorld(
		&game->world,
		game,
		entries,
		sizeof(entries) / sizeof(WorldEntry)
	);
}

void addSoldatoGroupWithLeader(Game * game, EntityId leader, int groupSize, Vector3 position, Vector3 spacing) {
    // Add leader.
    addEntryToWorld(&game->world, game, (WorldEntry){leader, position, QuaternionIdentity()});

    // Add soldato
    addEntityGroupToWorld(
        game,
        ENTITY_SOLDATO,
        groupSize,
        Vector3Add(position, spacing),
        spacing
    );
}
