#include "entities/antifaShip.h"
#include "entities/soldato.h"
#include "entities/caporale.h"
#include "entities/sergente.h"
#include "entities/maresciallo.h"
#include "entities/generale.h"
#include "entities/mussolini.h"
#include "entities/guidedMissile.h"
#include "entities/missile.h"

// Through the magic of this ugly file you can include all entities at once.
