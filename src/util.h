#include "gameCommon.h"

#ifndef UTIL_H
#define UTIL_H

// Bit shit.
#define SET_BIT(b, n) (b | (0x1 << n))
#define CLEAR_BIT(b, n) (b & ~(0x1 << n))
#define IS_BIT_SET(b, n) ((b >> n) & 0x1)
#define TOGGLE_BIT(b, n) (b ^ (0x1 << n))
#define HAS_FLAG(v, f) ((v & f) == f)

typedef struct AxisAngle {
	Vector3 axis;
	float angle;
} AxisAngle;

AxisAngle AxisAngleIdentity();

// boring math shit.
float signum(float n);

// Debugging stuff.
void printVector3(Vector3 v);
void printVector2(Vector2 v);

// Thank you chatgpt.
bool checkTriangleCollision3D(const Triangle3D triangleA, const Triangle3D triangleB, Vector3 normalA, Vector3 normalB);

void copyTriangle3D(Triangle3D a, const Triangle3D b);

void drawModelWireframe(Model model, Vector3 position, Quaternion rotation, Color color);

#endif
