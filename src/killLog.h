#include "gameCommon.h"
#include "entity.h"

#ifndef KILL_LOG_H
#define KILL_LOG_H

#define KILL_LOG_MAX 10
#define KILL_MESSAGE_MAX 100
#define KILL_LOG_FONT_SIZE 16

typedef struct Kill {
    EntityType type;
    EntityId id;
    EntityFingerprint fingerPrint;
    double timeAtKill;
} Kill;

typedef struct KillLog {
    Kill kills[KILL_LOG_MAX];
    size_t killCount;
} KillLog;

void initKillLog(KillLog * killLog);
void pushKill(KillLog * killLog, Entity entity);
void drawKillLog(KillLog * killLog, Vector2 position);
void resetKillLog(KillLog * killLog);

#endif
